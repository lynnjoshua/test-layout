import { Component, OnDestroy, OnInit } from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {filter} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.subscription = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      document.querySelector('.mat-sidenav-content').scrollTop = 0;
    });
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  onDeactivate() {
    // console.log('test');
    // console.log(document.body);
    // Alternatively, you can scroll to top by using this other call:
    // window.scrollTo(0, 0)
  }
}
